**This is Serenity Cucumber Framework for testing petstore.swagger.io API (Rest assured)**

**Capabilities:** 
- Run all the service Endpoint for pet, store and users.
- do REST API (GET/POST/PUT/DELETE) calls to predefined endpoints.
- check status code from response code, headers. 
- Assert validation added for each scenarios.
- check negative scenarios as well for all the service endpoints.
- Test cases are written in Cucumber/Gerkin laguage. 
- As a part of HTML reporting , Serenity has been configured to generate HTML report.

 
**Project Structure **

Runner file :  /serenity-cucumber-starter-master/src/test/java/starter/CucumberTestSuite.java
feature files : /serenity-cucumber-starter-master/src/test/resources/features/features
Step Definations : /serenity-cucumber-starter-master/src/test/java/starter/Stepdefinations
Methods : /serenity-cucumber-starter-master/src/test/java/starter/java_methods

**How to Run and generate report:**
- Prerequisites:maven3, java8 or greater 
- Run command from base project:mvn clean verify(this will run all the testcases from all the features)
- Html report is generated when running above command in target/site/serenity/index.html 

**Comments on Report :**
- For pet endpoint service , all the testcases are working as expected. Report attached. 
- For Store , testcases are working as expected.
- For user API endpoints, When trying to create a list of users API is throwing status code as 500 .When I searched about it, got to know that it is a server related issue which can be fixed only by swagger team itself. Snapshot attached. May be when ran after few days the issue might have fixed automatically hence my script will run as expected. 



