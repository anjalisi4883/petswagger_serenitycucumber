package starter.Stepdefinations;

import io.cucumber.java.en.Given;
import starter.java_methods.Store_methods;

public class Store_StepDefinations {

	
	@Given("I perform POST operation for adding an order for a pet")
	public void I_perform_POST_operation_for_adding_an_order_for_a_pet() {
		Store_methods.placeanorder();
	}
	
	@Given("I perform GET operation to find the order by orderID")
	public void I_perform_GET_operation_to_find_the_order_by_orderID() {
		Store_methods.FindPurchasebyID();
	}
	
	
	@Given("I perform GET operation to get the list of pet inventories by status") 	
	public void I_perform_GET_operation_to_get_the_list_of_pet_inventories_by_status() {
		Store_methods.StoreInventory();
	}
	
	
}
