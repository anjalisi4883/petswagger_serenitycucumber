package starter.Stepdefinations;

import io.cucumber.java.en.*;
import starter.java_methods.Pets_methods;


public class Pets_StepDefinitions {
	
	
	@Given("I perform POST operation for adding a pet to the store with name and ID")
	   public void I_perform_an_POST_operation_for_adding_a_pet_to_the_store_with_name_and_ID() {
		Pets_methods.addNewPet();
	}
	
	
	@Given("^I perform GET operation for finding the list of Pets by Status as\"(.*)\"$")
		public void Find_the_list_of_Pets_by_Status_as(String PetStatus)	{
			Pets_methods.findPet_status(PetStatus);
		}
			
	
	@Given("I perform PUT operation for updating an existing pet")
		public void I_perform_PUT_operation_for_updating_an_existing_pet() {
			Pets_methods.Update_existingPet();
	}
	
	@Given("I perform POST operation for uploading an image by PetID")
		public void I_perform_POST_operation_for_uploading_an_image_by_PetID()
		{
			Pets_methods.UploadanImage();
		}
	
	@Given("I perform GET operation to find the pet by petID")
		public void I_perform_GET_operation_to_find_the_pet_by_petID() {
		Pets_methods.SearchPetbyPetID();
	}
	
	@Given("I perform GET operation to find the non-registered pet")
		public void I_perform_GET_operation_to_find_the_non_registered_pet() {
		Pets_methods.Search_NonregisteredPet();
	}
	
	
	@Given("I perform DELETE operation to delete a pet from the store")
	public void I_perform_DELETE_operation_to_delete_a_pet_from_the_store() {
		Pets_methods.DeletePet();
	}
	
	@Given("I perform DELETE operatio to delete a pet not avaliable in store")
	public void I_perform_DELETE_operation_to_delete_a_pet_not_avaliable_in_store() {
		Pets_methods.Delete_NotAvaliablePet();
	}
	


}
