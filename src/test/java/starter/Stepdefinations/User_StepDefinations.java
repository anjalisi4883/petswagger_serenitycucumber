package starter.Stepdefinations;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.*;
import starter.java_methods.Users_methods;

public class User_StepDefinations {

	
	@Given("I perform POST operation to add a new user with input array")
	public void I_perform_POST_operation_to_add_a_new_user_with_input_array() {
		Users_methods.AddnewUser_withArray();
	}
	
	@Given("I perform POST operation to add a new user with input List")
	public void I_perform_POST_operation_to_add_a_new_user_with_input_List() {
		Users_methods.AddnewUser_withList();
	}
	
	@Given("I perform GET operation to get the details of user by entering the username")
	public void I_perform_GET_operation_to_get_the_details_of_user_by_entering_the_username() {
		Users_methods.Getuser_byUsername();
	}
	
	
	@Given("I perform GET operation to validate the status code with non-exist username")
	public void I_perform_GET_operation_to_validate_the_status_code_with_non_exist_username() {
		Users_methods.Get_nonExistuser();
	}
	
	@Given("I perform GET operation to validate the status code with invalid username")
	public void I_perform_GET_operation_to_validate_the_status_code_with_invalid_username() {
		Users_methods.Check_invalidusername();
	}
	
}
