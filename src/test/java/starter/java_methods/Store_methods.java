package starter.java_methods;

import org.json.simple.JSONObject;
import org.junit.Assert;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Store_methods {
	
	
	public static void placeanorder() {
		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json");
		JSONObject jsn = new JSONObject();
		jsn.put( "petId","100");
		jsn.put( "quantity", "1");
		request.body(jsn.toJSONString());
		Response res = request.post("https://petstore.swagger.io/v2/store/order");
		int Statuscode = res.getStatusCode();

		Assert.assertEquals(200, Statuscode);
		System.out.println("New Order placed  -Status code as " +Statuscode );
		String jsonString = res.asString();
		System.out.println(jsonString);

	}
	
	public static void FindPurchasebyID() {
	 
			Response response = RestAssured.given()
					.contentType(ContentType.JSON)
					.when()
					.get("https://petstore.swagger.io/v2/store/order/7")
					.thenReturn();
			int Statuscode = response.getStatusCode();
			Assert.assertEquals(200, Statuscode);
		}
	

	
	public static void StoreInventory() {
		
		Response response = RestAssured.given()
				.contentType(ContentType.JSON)
				.when()
				.get("https://petstore.swagger.io/v2/store/inventory")
				.thenReturn();
		int Statuscode = response.getStatusCode();
		Assert.assertEquals(200, Statuscode);
	}
		
	
		
	

}
