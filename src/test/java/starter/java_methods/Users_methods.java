package starter.java_methods;

import org.json.simple.JSONObject;
import org.junit.Assert;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Users_methods {


	public static void AddnewUser_withArray() {

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type","application/json");

		JSONObject jsn = new JSONObject();
		jsn.put( "username","Advent");
		jsn.put( "firstName", "Advent");
		jsn.put( "lastName","Bid");
		//jsn.put( "email", "Advent@gmail.com");
		//jsn.put( "password","Advent@123");
		//jsn.put( "phone", "123456789");
		request.body(jsn.toJSONString());
		Response res = request.post("https://petstore.swagger.io/v2/user/createWithArray");
		int Statuscode = res.getStatusCode();

		//Assert.assertEquals(200, Statuscode);
		System.out.println("New user registered with an input array list -Status code as " +Statuscode );
		String jsonString = res.asString();
		System.out.println(jsonString);


	}

	public static void AddnewUser_withList() {

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json");

		JSONObject jsn = new JSONObject();
		jsn.put( "username","Anna");
		jsn.put( "firstName", "Anna");
		jsn.put( "lastName","Hathway");
		//jsn.put( "email", "Anna@gmail.com");
		//jsn.put( "password","Anna@123");
		//jsn.put( "phone", "123456789");
		request.body(jsn.toJSONString());
		Response res = request.post("https://petstore.swagger.io/v2/user/createWithList");
		int Statuscode = res.getStatusCode();

		Assert.assertEquals(200, Statuscode);
		System.out.println("New user registered with an input array list -Status code as " +Statuscode );
		String jsonString = res.asString();
		System.out.println(jsonString);

	}

	public static void Getuser_byUsername() {
		Response response = RestAssured.given()
				.contentType(ContentType.JSON)
				.when()
				.get("https://petstore.swagger.io/v2/user/user1")
				.thenReturn();
		int Statuscode = response.getStatusCode();
		Assert.assertEquals(200, Statuscode);
	}
	
	public static void Get_nonExistuser() {
		Response response = RestAssured.given()
				.contentType(ContentType.JSON)
				.when()
				.get("https://petstore.swagger.io/v2/user/@@@")
				.thenReturn();
		int Statuscode = response.getStatusCode();
		Assert.assertEquals(404, Statuscode);
		System.out.println("For user not avaliable in database expected status code is"+Statuscode);
	}
	
	public static void Check_invalidusername() {
		Response response = RestAssured.given()
				.contentType(ContentType.JSON)
				.when()
				.get("https://petstore.swagger.io/v2/user/")
				.thenReturn();
		int Statuscode = response.getStatusCode();
		Assert.assertEquals(400, Statuscode);
		System.out.println("For invalid username"+Statuscode);
	}
}
