package starter.java_methods;


import static io.restassured.RestAssured.*;

import java.io.File;

import org.json.simple.JSONObject;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.response.Response;
import org.junit.Assert;

public class Pets_methods {

	private static final String BASE_URL= "https://petstore.swagger.io/v2";

	public static void addNewPet() {
		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json");
		JSONObject jsn = new JSONObject();
		jsn.put( "id","454545");
		jsn.put( "name", "Pigeon");
		request.body(jsn.toJSONString());
		Response res = request.post("https://petstore.swagger.io/v2/pet");
		int Statuscode = res.getStatusCode();

		Assert.assertEquals(200, Statuscode);
		System.out.println("New Pet registered with name -Status code as " +Statuscode );
		String jsonString = res.asString();
		System.out.println(jsonString);



	}
	public static void Update_existingPet() {

		RequestSpecification request = RestAssured.given();
		request.header("Content-Type", "application/json");
		JSONObject jsn = new JSONObject();
		jsn.put("id", "454545");
		jsn.put("name", "Pigeon23");
		request.body(jsn.toJSONString());
		Response res = request.put("https://petstore.swagger.io/v2/pet");
		int Statuscode = res.getStatusCode();

		Assert.assertEquals(200, Statuscode);
		System.out.println("Existing Pet updated" +Statuscode );
		String jsonString = res.asString();
		System.out.println(jsonString);

	}

	public static void findPet_status(String PetStatus) 
	{
		String PetURL= null;
		switch(PetStatus)
		{
		case "Available": 
			PetURL= "https://petstore.swagger.io/v2/pet/findByStatus?status=available";
			break;
		case "Pending":
			PetURL= "https://petstore.swagger.io/v2/pet/findByStatus?status=pending";
			break;
		case "sold":
			PetURL="https://petstore.swagger.io/v2/pet/findByStatus?status=sold"; 
			break;
		}
		System.out.println(PetURL);
		given()
		.contentType(ContentType.JSON)
		.when()
		.get(PetURL)
		.then()
		.statusCode(200).log().all();

	}

	public static void UploadanImage() {

		File file = new File("C:/Users/GOLU/Serenity workspace/serenity-cucumber-starter-master/src/test/resources/Images/this-im.jpg");

		Response response = RestAssured
				.given()
				.multiPart("file", file , "multipart/form-data")
				.post("https://petstore.swagger.io/v2/pet/454545/uploadImage")
				.thenReturn();

		int Statuscode = response.getStatusCode();
		Assert.assertEquals(200, Statuscode);
	}

	public static void SearchPetbyPetID() {

		Response response = RestAssured.given()
				.contentType(ContentType.JSON)
				.when()
				.get("https://petstore.swagger.io/v2/pet/454545")
				.thenReturn();
		int Statuscode = response.getStatusCode();
		Assert.assertEquals(200, Statuscode);

	}
	public static void Search_NonregisteredPet() {

		Response response = RestAssured.given()
				.contentType(ContentType.JSON)
				.when()
				.get("https://petstore.swagger.io/v2/pet/454545")
				.thenReturn();
		int Statuscode = response.getStatusCode();
		String message = response.asString();
		Assert.assertEquals(404, Statuscode);
		System.out.println("This is negative testcase and expected statuscode is 404 for nonregistered pet");

	}

	public static void DeletePet() {
		Response response = RestAssured.given()
				.contentType(ContentType.JSON)
				.when()
				.delete("https://petstore.swagger.io/v2/pet/454545")
				.thenReturn();
		int Statuscode = response.getStatusCode();
		Assert.assertEquals(200, Statuscode);

	}

	public static void Delete_NotAvaliablePet() {
		Response response = RestAssured.given()
				.contentType(ContentType.JSON)
				.when()
				.delete("https://petstore.swagger.io/v2/pet/454545")
				.thenReturn();
		int Statuscode = response.getStatusCode();
		Assert.assertEquals(404, Statuscode);

	}
	


}
