
Feature: Validate all services endpoint to test the Store-Access to Petstore orders 

Scenario: Place an order for a pet
Given I perform POST operation for adding an order for a pet

Scenario: Find purchase order by ID
Given I perform GET operation to find the order by orderID

Scenario: Returns pet inventories by status
Given I perform GET operation to get the list of pet inventories by status 
