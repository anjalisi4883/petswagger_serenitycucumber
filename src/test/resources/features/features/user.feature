Feature: Validate all services endpoint to test the User - operations about user

  Scenario: Create user with given input array
    Given I perform POST operation to add a new user with input array
    
 Scenario: Create user with given input list
    Given I perform POST operation to add a new user with input List

  Scenario: Get user by entering the username
    Given I perform GET operation to get the details of user by entering the username

  Scenario: Get user details by entering non-exist username- Negative Testcase
    Given I perform GET operation to validate the status code with non-exist username
  