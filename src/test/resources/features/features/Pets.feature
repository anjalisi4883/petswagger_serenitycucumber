Feature: Validate all services endpoint to test the pet- Everything about your pets

  Scenario: Add a new pet to the store
    Given I perform POST operation for adding a pet to the store with name and ID

  Scenario: Upload an Image to pet ID
    Given I perform POST operation for uploading an image by PetID

  Scenario: Update an existing pet
    Given I perform PUT operation for updating an existing pet

  Scenario: Find Pets by Status
    Given I perform GET operation for finding the list of Pets by Status as"Pending"

  Scenario: Find the pet by petID
    Given I perform GET operation to find the pet by petID

  Scenario: Delete a pet from the store
    Given I perform DELETE operation to delete a pet from the store

  Scenario: Delete a pet which is not avaliable in Store-Negative testcase
    Given I perform DELETE operatio to delete a pet not avaliable in store

  Scenario: Find the pet by petID which is not Registered yet-Negative tescase
    Given I perform GET operation to find the non-registered pet
